# AstroNNomy
Space yay!

**Sub_Images_LoTSS-DR1.ipynb** requires astropy, matplolib and numpy for visualisation. Written in python 3. Visualising is in the end of the notebook. Please, let me know if you would like to play with 'montage' and I can make one mosaic and the catalogues available. 

**AstroNNomy Folder** The dataset can be found here https://drive.google.com/drive/folders/1rXRe30HQMdrFiYQAvA42mIR7_nfcLHtH?usp=sharing. We can use this folder to put some other files, if necessary. 
